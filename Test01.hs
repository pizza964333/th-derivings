{-# LANGUAGE GADTs, TemplateHaskell #-}

module Test01 where

import Language.Haskell.TH
import Language.Haskell.TH.Syntax (Lift(..), showName)
import Data.List (intercalate, nub, elemIndex)

constrInstance :: Name -> Q [Dec]
constrInstance n = do
  i <- reify n
  case i of
    TyConI (DataD    _ n _ _ cs _) -> mkInstance n cs
    TyConI (NewtypeD _ n _ _ c  _) -> mkInstance n [c]
    _ -> return []
  where
    mkInstance n cs = do
      ds <- mapM (mkConstrData n) cs
      is <- mapM (mkConstrInstance n) cs
      return $ concat ds ++ concat is

mkConstrData :: Name -> Con -> Q [Dec]
mkConstrData dt (NormalC n _) = do a <- dataD (cxt []) (genName [dt, n]) [] Nothing [] []; return [a]
mkConstrData dt (GadtC ns _ _) = mapM (\n -> dataD (cxt []) (genName [dt, n]) [] Nothing [] []) ns
mkConstrData _ _ = return []

class Constructor a where
  conName :: a -> String

genName :: [Name] -> Name
genName = mkName . (++"_") . intercalate "_" . map nameBase

mkConstrInstance :: Name -> Con -> Q [Dec]
mkConstrInstance dt (GadtC ns _ _) = mapM (\n -> mkConstrInstanceWith dt (n) []) ns
mkConstrInstance _ _ = return []

mkConstrInstanceWith :: Name -> Name -> [Q Dec] -> Q Dec
mkConstrInstanceWith dt n extra =
  instanceD (cxt []) (appT (conT ''Constructor) (conT $ genName [dt, n]))
    (funD 'conName [clause [wildP] (normalB (stringE (nameBase n))) []] : extra)










