{-# LANGUAGE MultiParamTypeClasses, TypeFamilies, AllowAmbiguousTypes, GADTs, TypeOperators #-}

module Data.GeneralFunctor where

import Control.Arrow
import GHC.TypeLits

newtype FixF f a = InF { outF :: f (FixF f a) }

class KnownNat n => GeneralFunctor f n b c where
  type GetFunctor f n :: * -> *
  gfmap :: Arrow a => a b c -> a (GetFunctor f n b) (GetFunctor f n c)


  type Generalize 

  generalize :: f body name arg -> Generalize f body name arg
  specialize :: f body "general" () -> Specialize f body name arg


  gfmap :: (f b "general" () -> f b n c) -> a c d -> a (f b n c) (Genaralize (f b n d))

data a :. b

data Test a b where
  AAAA :: Int -> Test () Int -> Test () Int
  BBBB :: Int -> Double -> Test (Int :. Double :. ()) ()
  Pop :: Test (a :. b) c -> Term b (a :. c)

