{-# LANGUAGE GADTs, TemplateHaskell, KindSignatures, FlexibleInstances, MultiParamTypeClasses #-}

import Language.Haskell.TH hiding (Name)
import Language.Haskell.TH.Syntax (Lift(..), showName)
import Data.List (intercalate, nub, elemIndex)
import Test01
import Control.Monad
import Data.GeneralFunctor
import Data.Word
import GHC.TypeLits hiding (Symbol)
import qualified GHC.TypeLits as T

data Hints a
data Order
data Comm
data Symbol a
data Logic a
data Prefix a
data Unique
data Vacant
data Leaf
data Ident a
data Name a
data Exponential a
data MCNJ a b
data CAPS a
data AU
data MU

class Annotation a where

instance Annotation (Hints Order) where
instance Annotation (Hints Comm) where

class Vacantable a where

instance Vacantable Leaf   where
instance Vacantable Unique where
instance Vacantable a => Vacantable (Prefix a) where

class Uniqueable a where

instance Uniqueable Leaf where
instance Uniqueable a => Uniqueable (Prefix a) where

class Capsulation a where

instance Capsulation Leaf   where
instance Capsulation Unique where
instance Capsulation Vacant where
instance Capsulation a => Capsulation (Prefix a) where

class Replicable a where

instance Replicable MU where
instance Replicable a => Replicable (CAPS a) where

newtype FixF f a = InF { outF :: f (FixF f a) }

class Symbolable a where

instance KnownNat a    => Symbolable (Ident a) where
instance KnownSymbol a => Symbolable (Name  a) where

--instance Functor 

--instance OrderHints 

type Fix   a = FixF Term a
type TermF a = Term (Fix a)

data Term a where
   AU :: TermF (Logic AU)
   MU :: TermF (Logic MU)
   INVE, FAVO :: TermF (Logic a) -> TermF (Logic a)
   OFCO, WHNT :: TermF (Logic a) -> TermF (Logic (Exponential a))
   ANNO :: Annotation a => Term a -> TermF (Logic b) -> TermF (Logic b)
   MCNJ :: TermF (Logic a) -> TermF (Logic b) -> TermF (Logic (MCNJ a b))
   --ACNJ, ADSJ, MDSJ, ASEQ, MSEQ :: TermF (Logic b) -> TermF Logic -> TermF Logic
   Name  :: String -> TermF (Symbol (Leaf a))
   Ident :: Word64 -> TermF (Symbol (Leaf a))
   Symbol  :: Symbolable a => TermF (Symbol (Leaf a))

{-
   Prefix :: TermF (Symbol Leaf) -> TermF (Symbol a) -> TermF (Symbol (Prefix a))
   Unique :: Uniqueable a => TermF (Symbol a) -> TermF (Symbol Unique)
   Vacant :: Vacantable a => TermF (Symbol a) -> TermF (Symbol Vacant)
   CAPS :: Capsulation a => TermF (Symbol a) -> TermF (Logic b) -> TermF (Logic (CAPS b))
   REPL :: Replicable a => Integer -> TermF (Logic a) -> TermF (Logic a)
   EmptyHint :: TermF (Hints a)
   OrderHints :: { level :: Int, order :: Integer, commHints :: TermF (Hints Comm), nextOH :: TermF (Hints Order) } -> TermF (Hints Order)
   CommHints  :: { relation :: Int, nextCH :: TermF (Hints Comm) } -> TermF (Hints Comm)

instance GeneralFunctor Term (Fix Logic) (Fix Logic) where
  gfmap f (INVE a) = INVE $ outF $ f $ InF a
-}

$(constrInstance ''Term)









